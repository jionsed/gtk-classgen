NAME=gtk-classgen

install:
	sudo mkdir -p /usr/share/${NAME}/
	sudo cp gtk-classgen /usr/sbin/${NAME}
	sudo cp -rf ext /usr/share/${NAME}/ext
	sudo cp template.c /usr/share/${NAME}/template.c
	sudo cp template.h /usr/share/${NAME}/template.h
