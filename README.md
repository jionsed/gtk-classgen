Simple gtk3+ class generator for C

Requirements:

python3


## Installing

Run
```
sudo make install
```
to place gtk-classgen file in PATH (default: /usr/bin)

## Example usage


```
gtk-classgen my_class_name GtkApplicationWindow ./src/
```

Where:


my_class_name - Name for new  gtk3 component

GtkApplicationWindow - Parent gtk3 class

./src/ - destination directory
