#include <gtk/gtk.h>

#include "%{F}.h"

G_DEFINE_TYPE (%{N}, %{L}, GTK_TYPE_APPLICATION);

static void
activate (GApplication* app)
{
	G_APPLICATION_CLASS(%{L}_parent_class)->activate (app);
}

static void
startup (GApplication* app)
{
	G_APPLICATION_CLASS(%{L}_parent_class)->startup (app);
}

void
%{L}_init (%{N} *self)
{

}

void
%{L}_class_init (%{N}Class *class)
{
	G_APPLICATION_CLASS(class)->activate = activate;
	G_APPLICATION_CLASS(class)->startup = startup;
}

GApplication *
%{L}_new ()
{
	return g_object_new (%{C_TYPE},
		"application-id", "org.gnome.%{L}", NULL);
}
