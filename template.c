#include <gtk/gtk.h>

#include "%{F}.h"

G_DEFINE_TYPE (%{N}, %{L}, %{P_TYPE});

void
%{L}_init (%{N} *self)
{

}

void
%{L}_class_init (%{N}Class *klass)
{

}

GtkWidget *
%{L}_new ()
{
	return g_object_new (%{C_TYPE}, NULL);
}
