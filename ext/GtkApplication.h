#ifndef __%{C}_H__
#define __%{C}_H__

#include <gtk/gtk.h>

#define %{C_TYPE} %{L}_get_type()
#define %{C}(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), %{C_TYPE}, %{N}))
#define %{C}_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), %{C_TYPE}, %{N}Class))
#define %{IS_C}(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), %{C_TYPE}))
#define %{IS_C}_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), %{C_TYPE}))
#define %{C}_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), %{C_TYPE}, %{N}Class))

typedef struct _%{N} %{N};
typedef struct _%{N}Class %{N}Class;

struct _%{N} {
	GtkApplication parent;
};

struct _%{N}Class {
	GtkApplicationClass parent_class;
};

GType %{L}_get_type (void);

GApplication *%{L}_new (void);

#endif
